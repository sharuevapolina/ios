// Создайте по 2 enum с разным типом RawValue
enum Fruit: Int {
    case apple = 1
    case banana = 2
    case orange = 3
}

let myFruit: Fruit = .banana
print(myFruit.rawValue)

enum Color: String {
    case red = "FF0000"
    case green = "00FF00"
    case blue = "0000FF"
}

let myColor: Color = .green
print(myColor.rawValue)

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
enum Gender: String {
    case male = "Мужской"
    case female = "Женский"
}

enum Age: String {
    case under25 = "До 25 лет"
    case from25to40 = "От 25 до 40 лет"
    case from40to60 = "От 40 до 60 лет"
    case over60 = "Более 60 лет"
}

enum Experience: String {
    case upTo1Year = "До 1 года"
    case from1to3Years = "От 1 до 3 лет"
    case from3to5Years = "От 3 до 5 лет"
    case over5Years = "Более 5 лет"
}

struct EmployeeProfile {
    var gender: Gender
    var age: Age
    var experience: Experience
}

let employeeProfile = EmployeeProfile(gender: .male, age: .from25to40, experience: .from3to5Years)

print(employeeProfile.gender.rawValue) 
print(employeeProfile.age.rawValue)
print(employeeProfile.experience.rawValue)
// Создать enum со всеми цветами радуги
enum RainbowColor: String {
    case red = "Красный"
    case orange = "Оранжевый"
    case yellow = "Желтый"
    case green = "Зеленый"
    case blue = "Голубой"
    case indigo = "Синий"
    case violet = "Фиолетовый"
}

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
func printEnumCases() {
    enum Fruits {
        case Apple
        case Banana
    }
    
    enum Colors {
        case Green
        case Red
        case Blue
    }
    
    let myFruit = Fruits.Apple
    let myColor = Colors.Green
    
    switch myFruit {
    case .Apple:
        print("apple", terminator: " ")
    case .Banana:
        print("banana", terminator: " ")
    }
    
    switch myColor {
    case .Green:
        print("green", terminator: " ")
    case .Red:
        print("red", terminator: " ")
    case .Blue:
        print("blue", terminator: " ")
    }
}

printEnumCases()


// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
func getScoreValue(for score: Score) -> Int {
  switch score {
  case .excellent:
    return 5
  case .good:
    return 4
  case .satisfactory:
    return 3
  case .unsatisfactory:
    return 2
  }
}

enum Score: String {
  case excellent = "Отлично"
  case good = "Хорошо"
  case satisfactory = "Удовлетворительно"
  case unsatisfactory = "Неудовлетворительно"
}

let score = Score.good
print("\n\(getScoreValue(for: score))")

// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum
enum Car: String {
  case bmw = "BMW"
  case mercedes = "Mercedes"
  case audi = "Audi"
  case volkswagen = "Volkswagen"
  case toyota = "Toyota"
}

func printCarsInGarage(cars: [Car]) {
  print("Автомобили в гараже:")
  for car in cars {
    print(" - \(car.rawValue)")
  }
}

let carsInGarage: [Car] = [.bmw, .mercedes, .audi]
printCarsInGarage(cars: carsInGarage)
