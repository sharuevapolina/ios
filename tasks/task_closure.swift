// Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
let numbers = [5, 3, 1, 2, 4]

print(numbers.sorted(by: <))
print(numbers.sorted(by: >))

// Создать метод, который принимает имена друзей, после этого имена положить в массив
func readFriendNamesFromConsole() -> [String] {
    var friendNames: [String] = []
    
    repeat {
        print("Введите имя друга (или 1, чтобы закончить):")
        let input = readLine()!
        
        if input == "1" {
            break
        } else {
            friendNames.append(input)
        }
    } while true
    
    return friendNames
}

let friendNames = readFriendNamesFromConsole()
print(friendNames)

// Массив отсортировать по количеству букв в имени
print(friendNames.sorted { $0.count < $1.count }) 

// Создать словарь (Dictionary), где ключ - кол-во символов имени, а в значении - имя друга
func createFriendNameDictionary(friendNames: [String]) -> [Int: [String]] {
    var friendNameDictionary: [Int: [String]] = [:]
    
    for friendName in friendNames {
        let friendNameLength = friendName.count
        
        if friendNameDictionary[friendNameLength] == nil {
            friendNameDictionary[friendNameLength] = []
        }
        
        friendNameDictionary[friendNameLength]?.append(friendName)
    }
    
    return friendNameDictionary
}

let friendNameDictionary = createFriendNameDictionary(friendNames: friendNames)
print(friendNameDictionary)

// Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
func printDictionaryEntry(dictionary: [Int: [String]]) {
    print("Введите ключ:")
    guard let key = Int(readLine()!) else {
        print("Неверный ввод")
        return
    }

    if let values = dictionary[key] {
        print("Ключ: \(key)")
        print("Значение: \(values)")
    } else {
        print("Ключ не найден")
    }
}

printDictionaryEntry(dictionary: friendNameDictionary)

// Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль
func checkAndFillEmptyArrays(stringArray: inout [String], numberArray: inout [Int]) {
    if stringArray.isEmpty {
        print("Введите строку:")
        if let input = readLine() {
            stringArray.append(input)
        }
    }
    
    if numberArray.isEmpty {
        print("Введите число:")
        if let input = readLine() {
            if let number = Int(input) {
                numberArray.append(number)
            }
        }
    }
    
    print("Строковый массив: \(stringArray)")
    print("Числовой массив: \(numberArray)")
}


var stringArray: [String] = []
var numberArray: [Int] = []

print("Введите строки (завершите вводом пустой строки):")
while let input = readLine() {
    if input.isEmpty {
        break
    }
    stringArray.append(input)
}

print("Введите числа (завершите вводом пустой строки):")
while let input = readLine() {
    if input.isEmpty {
        break
    }
    if let number = Int(input) {
        numberArray.append(number)
    }
}

checkAndFillEmptyArrays(stringArray: &stringArray, numberArray: &numberArray)