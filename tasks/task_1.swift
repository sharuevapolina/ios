struct Person {
    var name: String
    var age: Int
}

extension String.StringInterpolation {
    mutating func appendInterpolation(_ person: Person) {
        appendLiteral("I'm \(person.name) and I'm \(person.age) years old.")
    }
}

let introduce = Person(name: "Polina", age: 21)
let task = 1

print("Task \(task): \(introduce)")
