import Foundation 

// Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

//Создать массив элементов 'название месяцов' содержащий названия месяцев
let nameMonth = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь","Декабрь"]

// Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
print("Кол-во дней в месяцах:")
for i in daysInMonth{
    print(i, terminator: " ")
}

// Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней

print("\nКол-во дней в месяцах с указанием названия месяца:")
for (index, value) in nameMonth.enumerated(){
    print(value, daysInMonth[index])
}

// Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
var tuplesMonth = [(month: String, days: Int)]();

for (index, value) in nameMonth.enumerated() {
    tuplesMonth.append((value, daysInMonth[index]))
}

print("Кол-во дней в месяцах с указанием названия месяца (с использованием кортежей):")
for i in tuplesMonth{
    print(i.0, i.1)
}

// Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
print("\nКол-во дней в месяцах с указанием названия месяца (с использованием кортежей) в обратном порядке:")
for i in tuplesMonth.reversed(){
    print(i.0, i.1)
}

// Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
var flag = 0
var month = 0
var day = 0

repeat{
print("Введите номер месяца:", terminator: " ")
if let typed = readLine() {
  if let value = Int(typed){
      month = value
      if month > 0 &&  month < 13{
      flag = 1
      }
      else{
          print("Введите корректное значение!")
      }
  }
  else {
      print("Введите корректное значение!")
  }
}
} while flag == 0 

flag = 0
repeat{
print("Введите день:", terminator: " ")
if let typed = readLine() {
  if let value = Int(typed){
      day = value
      if day > 0 && day <= daysInMonth[month - 1]{
      flag = 1
      }
      else{
          print("Введите корректное значение!")
      }
  }
  else {
      print("Введите корректное значение!")
  }
}
} while flag == 0

print("Кол-во дней до введенной даты от начала года: \(daysInMonth[0...month - 2].reduce(0, +) + day - 1)")
