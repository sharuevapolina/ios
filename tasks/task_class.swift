//Создать класс родитель и 2 класса наследника
class Person {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func introduce() {
        print("Hello, my name is \(name).")
    }
}

class Student: Person {
    func study() {
        print("I'm studying.")
    }
}

class Teacher: Person {
    var subjectTaught: String
    
    init(name: String, subjectTaught: String) {
        self.subjectTaught = subjectTaught
        super.init(name: name)
    }
    
    func teach() {
        print("I'm teaching \(subjectTaught).")
    }
}

let person = Person(name: "Maria")
person.introduce()

let student = Student(name: "Anna")
student.introduce()
student.study()

let teacher = Teacher(name: "Elena", subjectTaught: "Math")
teacher.introduce()
teacher.teach()



//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
    var width: Double
    var height: Double
    var isDestroyed: Bool = false
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("House created. Area: \(area) square units.")
    }
    
    func destroy() {
        isDestroyed = true
        print("House destroyed.")
    }
}

let myHouse = House(width: 10.0, height: 5.0)
myHouse.create()
myHouse.destroy()


//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class Student1 {
    var name: String
    var age: Int
    var gradePointAverage: Double
    
    init(name: String, age: Int, gradePointAverage: Double) {
        self.name = name
        self.age = age
        self.gradePointAverage = gradePointAverage
    }
}

class StudentManager {
    var students: [Student1]
    
    init(students: [Student1]) {
        self.students = students
    }
    
    func sortStudentsByName() {
        students.sort { $0.name < $1.name }
    }
    
    func sortStudentsByAge() {
        students.sort { $0.age < $1.age }
    }
    
    func sortStudentsByGPA() {
        students.sort { $0.gradePointAverage > $1.gradePointAverage }
    }
}

let student1 = Student1(name: "Алексей", age: 20, gradePointAverage: 3.5)
let student2 = Student1(name: "Мария", age: 22, gradePointAverage: 3.8)
let student3 = Student1(name: "Иван", age: 19, gradePointAverage: 3.2)

let manager = StudentManager(students: [student1, student2, student3])

manager.sortStudentsByName()
print("Сортировка по имени:")
for student in manager.students {
    print(student.name)
}

manager.sortStudentsByAge()
print("\nСортировка по возрасту:")
for student in manager.students {
    print("\(student.name) - \(student.age) лет")
}

// Сортировка по среднему баллу
manager.sortStudentsByGPA()
print("\nСортировка по среднему баллу:")
for student in manager.students {
    print("\(student.name) - Средний балл: \(student.gradePointAverage)")
}


//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
// Структура Point
struct Point {
    var x: Int
    var y: Int
}

// Класс PointClass
class PointClass {
    var x: Int
    var y: Int
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}

// Пример использования
var point1 = Point(x: 1, y: 2)
var point2 = point1 // Создается копия point1

var pointClass1 = PointClass(x: 1, y: 2)
var pointClass2 = pointClass1 // Создается ссылка на существующий объект

// Изменение значений для структуры Point
point1.x = 3
print("point1: \(point1.x), \(point1.y)") // Выведет "point1: 3, 2"
print("point2: \(point2.x), \(point2.y)") // Выведет "point2: 1, 2"

// Изменение значений для класса PointClass
pointClass1.x = 3
print("pointClass1: \(pointClass1.x), \(pointClass1.y)") // Выведет "pointClass1: 3, 2"
print("pointClass2: \(pointClass2.x), \(pointClass2.y)") // Выведет "pointClass2: 3, 2"

/* 
Различия:
-  Классы поддерживают наследование, тогда как структуры этого не делают
- При присваивании структуры создается копия значений, в то время как при присваивании класса создается копия ссылки на объект.
- Свойства структуры (struct) не могут быть изменены, если экземпляр структуры объявлен как константа (let), в то время как свойства класса (class) могут быть изменены независимо от того, объявлен объект как константу или переменную.
*/

/* - Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
- Сохраняйте комбинации в массив
- Если выпала определённая комбинация - выводим соответствующую запись в консоль
- Результат в консоли примерно такой: 'У вас бубновый стрит флеш'. */

// В данном примере будем проверять наличие комбинации: Пара
struct Card {
    let value: String
    let suit: String
}

class PokerHandEvaluator {
    var hand: [Card] = []
    
    func addCard(value: String, suit: String) {
        let card = Card(value: value, suit: suit)
        hand.append(card)
    }
    
    func determineHand() -> String {
        var valueCounts: [String: Int] = [:]
        for card in hand {
            if let count = valueCounts[card.value] {
                valueCounts[card.value] = count + 1
            } else {
                valueCounts[card.value] = 1
            }
        }
        
        for (value, count) in valueCounts {
            if count >= 2 {
                return "Есть пара карт с достоинством \(value)"
            }
        }
        
        return "Нет известной комбинации"
    }
}

let evaluator = PokerHandEvaluator()

let suits = ["Пики", "Трефы", "Червы", "Бубны"]
let values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Валет", "Дама", "Король", "Туз"]

for _ in 1...5 {
    let randomSuit = suits.randomElement()!
    let randomValue = values.randomElement()!
    evaluator.addCard(value: randomValue, suit: randomSuit)
}

let handName = evaluator.determineHand()
print("Ваша комбинация:")
for card in evaluator.hand {
    print("\(card.value) \(card.suit)")
}
print("Результат: \(handName)")

