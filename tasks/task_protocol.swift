import Foundation
// Реализовать структуру IOSCollection и создать в ней copy on write
func address(of object: UnsafeRawPointer) -> String{
    let addr = Int(bitPattern: object)
    return String(format: "%p", addr)
}

func address(off value: AnyObject) -> String {
    return "\(Unmanaged.passUnretained(value).toOpaque())"
}

struct Identifier{
    var id = 1
}

class Ref<T> {
    var value: T
    init(value: T) {
        self.value = value
    }
}

struct IOSCollection<T> {
    var ref: Ref<T>
    init(value: T) {
        self.ref = Ref(value: value)
    }

    var value: T {
        get {
            ref.value
        }
        set {
            guard (isKnownUniquelyReferenced(&ref)) else {
                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}

var id = Identifier()
var collection1 = IOSCollection(value: id)
var collection2 = collection1

print(address(off: collection1.ref))
print(address(off: collection2.ref))

collection2.value.id = 12

print(address(off: collection1.ref))
print(address(off: collection2.ref))

// Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол
protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

let hotelAlfa = HotelAlfa(roomCount: 50)
print(hotelAlfa.roomCount)

// Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCoub = 4
diceCoub.numberDice


// Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство
protocol SomeProtocol {
    var property1: Int { get }
    var property2: String? { get set }
    
    func someMethod()
}

class SomeClass: SomeProtocol {
    var property1: Int = 10
    var property2: String?
    
    func someMethod() {
        print("Выполнение метода someMethod")
    }
}

let someObject = SomeClass()
print(someObject.property1)
someObject.someMethod() 


// Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
// Компании подключаем два этих протокола
// Задача: вывести в консоль сообщения - 'разработка началась. пишем код <такой-то>' и 'работа закончена. Сдаю в тестирование', попробуйте обработать крайние случаи.
protocol Coding {
    var time: Int { get }
    var linesOfCode: Int { get }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol Stoppable {
    func stopCoding()
}

enum Platform {
    case ios
    case android
    case web
}

class Company: Coding, Stoppable {
    var numberOfProgrammers: Int
    var specializations: [Platform]
    
    init(numberOfProgrammers: Int, specializations: [Platform]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }
    
    var time: Int = 0
    var linesOfCode: Int = 0
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        print("Разработка началась. Пишем код для платформы \(platform) с участием \(numberOfSpecialist) специалистов.")
        time += 1
        linesOfCode += 1000
    }
    
    func stopCoding() {
        if time > 0 {
            print("Работа закончена. Сдаю в тестирование.")
            time = 0
        } else {
            print("Нет начатой разработки для остановки.")
        }
    }
}

let company = Company(numberOfProgrammers: 50, specializations: [.ios, .android])
company.writeCode(platform: .ios, numberOfSpecialist: 5)
company.stopCoding()
company.stopCoding()
